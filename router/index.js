import express from 'express';
export const router = express.Router();

router.get("/", (req, res) => {

    const params = {
        numero: req.query.numero,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        servicio: req.query.servicio,
        kwConsumidos: req.query.kwConsumidos,
        isPost: false,
    };

    res.render("index", params);
});
  
router.post("/", (req, res) => {

    const precios = [1.08, 2.5, 3.0];

    const { numero, nombre, domicilio, servicio, kwConsumidos } = req.body;
    const precioKw = precios[servicio * 1];
    const tipoDeServicio = servicio == 0 ? 'Domestico' :  servicio == 1 ? 'Comercial' : 'Industrial'
    const subtotal = precioKw * kwConsumidos;

    // Calcular el descuento
    let descuento = 0;
    if (kwConsumidos <= 1000) {
        descuento = 0.1;
    } else if (kwConsumidos > 1000 && kwConsumidos <= 10000) {
        descuento = 0.2;
    } else {
        descuento = 0.5;
    }

    const descuentoAplicado = subtotal * descuento;
    const impuesto = 0.16 * subtotal;
    const total = subtotal + impuesto;
    const subtotalConDescuento = subtotal + impuesto - descuentoAplicado;

    const params = {
        numero,
        nombre,
        domicilio,
        servicio: tipoDeServicio,
        kwConsumidos,
        precioKw,
        subtotal,
        descuento: descuentoAplicado, 
        impuesto,
        subtotalConDescuento,
        total,
        isPost: true,
    };
    
    res.render("index", params);

});

export default { router };